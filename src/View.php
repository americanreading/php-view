<?php

namespace AmericanReading\View;

/**
 * Merges an associative array with a template to generate a string.
 */
interface View
{
    public function render(array $context): string;
}
