<?php

namespace AmericanReading\View;

/**
 * Provides a View for a given template.
 */
interface ViewFactory
{
    /**
     * @param string $template
     * @return View
     * @throws \OutOfBoundsException
     *     $template is invalid
     */
    public function getView(string $template): View;
}
